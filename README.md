# totp
> a totp validator

Created for the sake of learning (and memes)

[![Build status](https://gitlab.com/cosban/totp/badges/master/build.svg)](https://gitlab.com/cosban/totp/commits/master) 
[![GoDoc](https://godoc.org/gitlab.com/cosban/totp?status.svg)](https://godoc.org/gitlab.com/cosban/totp)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

## About

This is a TOTP (Time-based One Time Passwords) validation implementation for golang. It has been tested against the Google Authenticator app and should 
function well with others too.

You may also use this library to generate TOTPs. It is not recommended that you use this in place of a regular, hashed password.

## Usage

The easiest way to use this library is to supply the necessary arguments to `Validate` or `GenerateCode`.