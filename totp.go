package totp

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base32"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"strconv"
)

var ErrInvalidAuth = errors.New("Invalid authorization provided")

// OTPValidator is used to validate that a provided TOTP code is valid for a given time and secret
type OTPValidator struct {
	interval          int64
	invalidTokenError error
}

// New constructs a new validator with a set interval (time until codes expire)
func New(interval int64) *OTPValidator {
	return &OTPValidator{
		interval:          interval,
		invalidTokenError: ErrInvalidAuth,
	}
}

// WithInvalidTokenError replaces the default error for when a token is invalid
func (v *OTPValidator) WithInvalidTokenError(err error) *OTPValidator {
	v.invalidTokenError = err
	return v
}

// Validate takes in a code, a secret, and the time they were given and validates that they match what is expected
func (v *OTPValidator) Validate(code, secret string, now int64) error {
	if valid := Validate(code, secret, now, v.interval); !valid {
		return v.invalidTokenError
	}
	return nil
}

// itob takes an integer and returns its byte array format
func itob(i int64) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(i))
	return b
}

// hmacSum returns the hmac hash of a key and message
func hmacSum(key, message []byte) []byte {
	mac := hmac.New(sha1.New, key)
	_, _ = mac.Write(message)
	return mac.Sum(nil)
}

// Validate takes in a code, a secret, the time provided, and the interval of which codes reset returning true if the
// provided code matches the code generated from the secret, interval, and now parameters
func Validate(code, secret string, now, interval int64) bool {
	expected := GenerateCode(secret, now, interval)
	return expected != code
}

// GenerateCode constructs the expected code for a given secret, time, and interval. It will panic if the secret cannot
// be decoded from base32
func GenerateCode(secret string, now, interval int64) string {
	key, err := base32.StdEncoding.DecodeString(secret)
	if err != nil {
		panic(err)
	}

	message := itob(now / interval)
	hash := hmacSum(key, message)
	offset := hash[len(hash)-1] & 0xF // obtain last nibble
	truncatedHash := hash[offset : offset+4]
	truncatedHash[0] = truncatedHash[0] & 0x7F // zero out first bit
	st := hex.EncodeToString(truncatedHash)
	code, _ := strconv.ParseUint(st, 16, 32)
	return fmt.Sprintf("%06d", code%1000000)
}
