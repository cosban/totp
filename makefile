PROJECT := totp
PKG := "gitlab.com/cosban/$(PROJECT)"
PKG_LIST := $(shell go list $(PKG)/... | grep -v /vendor/)

.PHONY: all build clean test

all: build

test:
	@GO111MODULE=on go test -short -v $(PKG_LIST)

race:
	@GO111MODULE=on go test -v -race -short $(PKG_LIST)

build:
	@go build -i -v $(PKG)

clean:
	@rm -f $(PROJECT)