package totp

import (
	"testing"
	"time"

	"github.com/cosban/assert"
)

var (
	secret         = "MFSE2QSRMJVWG52YOB5GIYKXOJVVK42V"
	expected       = "857907"
	unixTime       = time.Date(2017, time.July, 27, 19, 39, 1, 0, time.UTC).Unix()
	interval int64 = 30
	totp           = New(interval)
)

func TestObtainTokenIsConsistent(t *testing.T) {
	assert := assert.New(t)

	actual := GenerateCode(secret, unixTime, interval)
	assert.Equals(expected, actual)
}

func TestObtainTokenChangesAfterInterval(t *testing.T) {
	assert := assert.New(t)

	actual := GenerateCode(secret, unixTime+interval, interval)
	assert.NotEquals(expected, actual)
}

func TestObtainTokenChangesFromPreviousInterval(t *testing.T) {
	assert := assert.New(t)

	actual := GenerateCode(secret, unixTime-interval, interval)
	assert.NotEquals(expected, actual)
}
